package com.example.appdemorain;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.icu.text.NumberFormat;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    private Button makeItRain;
    private TextView moneyValue;
    private int moneyCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        makeItRain = findViewById(R.id.buttonMakeItRain);
        moneyValue = findViewById(R.id.moneyValue);
//        moneyValue.setText(R.string.test);
//        makeItRain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("MainActivity", "onClick: Make it rain");
//            }
//        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void showMoney(View view) {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        moneyCounter = moneyCounter + 1000;
        moneyValue.setText(String.valueOf(numberFormat.format(moneyCounter)));
        // change color
        switch (moneyCounter) {
            case 20000:
                moneyValue.setTextColor(Color.RED);
                break;

            case 30000:
                moneyValue.setTextColor(Color.BLUE);
                break;

            case 40000:
                moneyValue.setTextColor(Color.MAGENTA);
                break;

            default:
                moneyValue.setTextColor(Color.GREEN);
        }
        Log.d("MainActivity", "onClick: " + moneyCounter);

    }

    public void showInfo(View view) {
//        Toast.makeText(MainActivity.this, R.string.app_info, Toast.LENGTH_SHORT).show();
        Snackbar.make(moneyValue, R.string.app_info, Snackbar.LENGTH_LONG)
                .setAction("More", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("snack", "showInfo: snackbar");
                    }
                })
                .show();
    }
}
